" ultisnips
" https://github.com/SirVer/ultisnips
"

set rtp+=~/.vim/my-snippets/
let g:UltiSnipsExpandTrigger = '<tab>'
let g:UltiSnipsJumpForwardTrigger = '<tab>'
let g:UltiSnipsJumpBackwardTrigger = '<s-tab>'
