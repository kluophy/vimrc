" vimtex
" https://github.com/lervag/vimtex
"
let g:tex_flavor='latex'
"let g:vimtex_view_method='zathura'
"let g:vimtex_view_method='mupdf'
"let g:vimtex_view_method='mupdf-x11'
let g:vimtex_view_method='general'
"let g:vimtex_view_general_viewer = 'mupdf'

let g:vimtex_quickfix_mode=0
set conceallevel=1
let g:tex_conceal='abdmg'
