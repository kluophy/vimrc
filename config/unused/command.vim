"============== Custom Mappings ===============
" general mapping
"nmap <C-Tab> :tabnext<CR>
"nmap <C-S-Tab> :tabprevious<CR>
"map <C-S-Tab> :tabprevious<CR>
"map <C-Tab> :tabnext<CR>
"imap <C-S-Tab> <ESC>:tabprevious<CR>
"imap <C-Tab> <ESC>:tabnext<CR>
"noremap <F7> :set expandtab!<CR>
"nmap <Leader>h :tabnew %:h<CR>
