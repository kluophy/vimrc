source ~/.vim/config/plugin-config/airline.vim
source ~/.vim/config/plugin-config/vimtex.vim
source ~/.vim/config/plugin-config/ultisnips.vim
source ~/.vim/config/plugin-config/nerdtree.vim
